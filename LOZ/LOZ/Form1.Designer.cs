﻿namespace LOZ
{
    partial class mapa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mapa));
            this.Link = new System.Windows.Forms.PictureBox();
            this.roca1 = new System.Windows.Forms.PictureBox();
            this.espada = new System.Windows.Forms.PictureBox();
            this.enemigo1 = new System.Windows.Forms.PictureBox();
            this.lblArmado = new System.Windows.Forms.Label();
            this.lblInstrucciones = new System.Windows.Forms.Label();
            this.lblPuntos = new System.Windows.Forms.Label();
            this.enemigo2 = new System.Windows.Forms.PictureBox();
            this.roca2 = new System.Windows.Forms.PictureBox();
            this.roca4 = new System.Windows.Forms.PictureBox();
            this.roca3 = new System.Windows.Forms.PictureBox();
            this.roca5 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.Link)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roca1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.espada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemigo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemigo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roca2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roca4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roca3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roca5)).BeginInit();
            this.SuspendLayout();
            // 
            // Link
            // 
            this.Link.Image = ((System.Drawing.Image)(resources.GetObject("Link.Image")));
            this.Link.Location = new System.Drawing.Point(6, 77);
            this.Link.Name = "Link";
            this.Link.Size = new System.Drawing.Size(42, 39);
            this.Link.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Link.TabIndex = 0;
            this.Link.TabStop = false;
            // 
            // roca1
            // 
            this.roca1.Image = ((System.Drawing.Image)(resources.GetObject("roca1.Image")));
            this.roca1.Location = new System.Drawing.Point(254, 95);
            this.roca1.Name = "roca1";
            this.roca1.Size = new System.Drawing.Size(43, 39);
            this.roca1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.roca1.TabIndex = 1;
            this.roca1.TabStop = false;
            // 
            // espada
            // 
            this.espada.Image = ((System.Drawing.Image)(resources.GetObject("espada.Image")));
            this.espada.Location = new System.Drawing.Point(12, 329);
            this.espada.Name = "espada";
            this.espada.Size = new System.Drawing.Size(27, 29);
            this.espada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.espada.TabIndex = 2;
            this.espada.TabStop = false;
            // 
            // enemigo1
            // 
            this.enemigo1.Image = ((System.Drawing.Image)(resources.GetObject("enemigo1.Image")));
            this.enemigo1.Location = new System.Drawing.Point(343, 214);
            this.enemigo1.Name = "enemigo1";
            this.enemigo1.Size = new System.Drawing.Size(43, 39);
            this.enemigo1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.enemigo1.TabIndex = 3;
            this.enemigo1.TabStop = false;
            // 
            // lblArmado
            // 
            this.lblArmado.AutoSize = true;
            this.lblArmado.Location = new System.Drawing.Point(3, 3);
            this.lblArmado.Name = "lblArmado";
            this.lblArmado.Size = new System.Drawing.Size(96, 13);
            this.lblArmado.TabIndex = 4;
            this.lblArmado.Text = "Heroe: Desarmado";
            // 
            // lblInstrucciones
            // 
            this.lblInstrucciones.AutoSize = true;
            this.lblInstrucciones.Location = new System.Drawing.Point(196, 3);
            this.lblInstrucciones.Name = "lblInstrucciones";
            this.lblInstrucciones.Size = new System.Drawing.Size(70, 13);
            this.lblInstrucciones.TabIndex = 5;
            this.lblInstrucciones.Text = "Instrucciones";
            // 
            // lblPuntos
            // 
            this.lblPuntos.AutoSize = true;
            this.lblPuntos.Location = new System.Drawing.Point(420, 5);
            this.lblPuntos.Name = "lblPuntos";
            this.lblPuntos.Size = new System.Drawing.Size(35, 13);
            this.lblPuntos.TabIndex = 6;
            this.lblPuntos.Text = "label1";
            // 
            // enemigo2
            // 
            this.enemigo2.Image = ((System.Drawing.Image)(resources.GetObject("enemigo2.Image")));
            this.enemigo2.Location = new System.Drawing.Point(124, 272);
            this.enemigo2.Name = "enemigo2";
            this.enemigo2.Size = new System.Drawing.Size(43, 39);
            this.enemigo2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.enemigo2.TabIndex = 7;
            this.enemigo2.TabStop = false;
            // 
            // roca2
            // 
            this.roca2.Image = ((System.Drawing.Image)(resources.GetObject("roca2.Image")));
            this.roca2.Location = new System.Drawing.Point(72, 44);
            this.roca2.Name = "roca2";
            this.roca2.Size = new System.Drawing.Size(43, 39);
            this.roca2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.roca2.TabIndex = 8;
            this.roca2.TabStop = false;
            // 
            // roca4
            // 
            this.roca4.Image = ((System.Drawing.Image)(resources.GetObject("roca4.Image")));
            this.roca4.Location = new System.Drawing.Point(72, 198);
            this.roca4.Name = "roca4";
            this.roca4.Size = new System.Drawing.Size(43, 39);
            this.roca4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.roca4.TabIndex = 9;
            this.roca4.TabStop = false;
            // 
            // roca3
            // 
            this.roca3.Image = ((System.Drawing.Image)(resources.GetObject("roca3.Image")));
            this.roca3.Location = new System.Drawing.Point(381, 44);
            this.roca3.Name = "roca3";
            this.roca3.Size = new System.Drawing.Size(43, 39);
            this.roca3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.roca3.TabIndex = 10;
            this.roca3.TabStop = false;
            // 
            // roca5
            // 
            this.roca5.Image = ((System.Drawing.Image)(resources.GetObject("roca5.Image")));
            this.roca5.Location = new System.Drawing.Point(343, 259);
            this.roca5.Name = "roca5";
            this.roca5.Size = new System.Drawing.Size(43, 39);
            this.roca5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.roca5.TabIndex = 11;
            this.roca5.TabStop = false;
            // 
            // mapa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(491, 370);
            this.Controls.Add(this.roca5);
            this.Controls.Add(this.roca3);
            this.Controls.Add(this.roca4);
            this.Controls.Add(this.roca2);
            this.Controls.Add(this.enemigo2);
            this.Controls.Add(this.lblPuntos);
            this.Controls.Add(this.lblInstrucciones);
            this.Controls.Add(this.lblArmado);
            this.Controls.Add(this.enemigo1);
            this.Controls.Add(this.espada);
            this.Controls.Add(this.roca1);
            this.Controls.Add(this.Link);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(507, 409);
            this.MinimumSize = new System.Drawing.Size(507, 409);
            this.Name = "mapa";
            this.Text = "LOZ";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.Link)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roca1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.espada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemigo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemigo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roca2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roca4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roca3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roca5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Link;
        private System.Windows.Forms.PictureBox roca1;
        private System.Windows.Forms.PictureBox espada;
        private System.Windows.Forms.PictureBox enemigo1;
        private System.Windows.Forms.Label lblArmado;
        private System.Windows.Forms.Label lblInstrucciones;
        private System.Windows.Forms.Label lblPuntos;
        private System.Windows.Forms.PictureBox enemigo2;
        private System.Windows.Forms.PictureBox roca2;
        private System.Windows.Forms.PictureBox roca4;
        private System.Windows.Forms.PictureBox roca3;
        private System.Windows.Forms.PictureBox roca5;
    }
}

