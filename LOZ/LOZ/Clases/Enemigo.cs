﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace LOZ.Clases
{
    class Enemigo
    {
        bool vivo;

        public Enemigo()
        {
            Vivo = true;
        }

        public bool Vivo { get => vivo; set => vivo = value; }

        public bool matarEnemigo()
        {

            return vivo = false;
        }

        public void checarEnemigo(PictureBox Link, PictureBox enemigo1)
        {
      
            enemigo1.Visible = Vivo;
       
        }
        public void checarEnemigo2(PictureBox Link, PictureBox enemigo2)
        {
            enemigo2.Visible = Vivo;

        }
    }
}
