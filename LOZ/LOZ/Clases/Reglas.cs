﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace LOZ.Clases
{
    class Reglas
    {
        public void checarGanador(Heroe heroe, Label lblinstrucciones)
        {
            if(heroe.Puntos == 20)
            {
                lblinstrucciones.Text = "GANASTE";
            }
        }

        public static void heroePerdido(PictureBox Link)
        {

            if (Link.Left <= -4)
            {
                Link.Left += 10;
            }
            if (Link.Left >= 446)
            {
                Link.Left -= 10;
            }
            if (Link.Top <= 17)
            {
                Link.Top += 10;
            }
            if (Link.Top >= 327)
            {
                Link.Top -= 10;
            }
        }
        

        public static void recogerArma(object sender, KeyEventArgs e, PictureBox Link, PictureBox espada, Heroe heroe, Label lblArmado, Label lblInstrucciones)
        {
            if (Link.Bounds.IntersectsWith(espada.Bounds))
            {
                heroe.armarHeroe();
                lblArmado.Text = "Heroe: Armado";
                lblInstrucciones.Text = "Mata a los enemigos";
                espada.Visible = false;

            }
        }

        public static bool revRoca(PictureBox Link, PictureBox roca1, PictureBox roca2, PictureBox roca3, PictureBox roca4, PictureBox roca5)
        {
            if (Link.Bounds.IntersectsWith(roca1.Bounds) || Link.Bounds.IntersectsWith(roca2.Bounds) || Link.Bounds.IntersectsWith(roca3.Bounds) || Link.Bounds.IntersectsWith(roca4.Bounds) || Link.Bounds.IntersectsWith(roca5.Bounds))
            {
                return true;
            }
            return false;
        }

        public static bool revEnemigo(PictureBox Link, PictureBox enemigo1, PictureBox enemigo2, Heroe heroe, Enemigo enemigo, Enemigo enemigoSeg)
        {
            if (Link.Bounds.IntersectsWith(enemigo1.Bounds) && enemigo.Vivo == true && heroe.Arma == false)
            {
                return true;
                

            }else if(Link.Bounds.IntersectsWith(enemigo2.Bounds) && enemigoSeg.Vivo==true && heroe.Arma == false)
            {
                return true;
            }
            return false;
           
        }

        public static bool revEnemigoArma(PictureBox Link, PictureBox enemigo1, PictureBox enemigo2, Heroe heroe, Enemigo enemigo)
        {
            if (Link.Bounds.IntersectsWith(enemigo1.Bounds) && enemigo.Vivo == true && heroe.Arma == true)
            {
                
                return true;
            }
            return false;
        }

        public static bool revEnemigo2Arma(PictureBox Link, PictureBox enemigo1, PictureBox enemigo2, Heroe heroe, Enemigo enemigoSeg)
        {
            if (Link.Bounds.IntersectsWith(enemigo2.Bounds) && enemigoSeg.Vivo == true && heroe.Arma == true)
            {
                return true;
            }
            return false;
        }

        public static void colisionD(PictureBox Link, PictureBox enemigo1, PictureBox enemigo2, PictureBox roca1, PictureBox roca2, PictureBox roca3, PictureBox roca4, PictureBox roca5, Heroe heroe, Enemigo enemigo, Enemigo enemigoSeg, Label lblPuntos)
        {
            if (revRoca(Link, roca1, roca2,roca3,roca4,roca5) == true)
            {
                Link.Left +=10;
            }
            if (revEnemigo(Link, enemigo1, enemigo2, heroe, enemigo, enemigoSeg) == true)
            {
                Link.Left +=10;
            }
            if (revEnemigoArma(Link, enemigo1, enemigo2, heroe, enemigo) == true)
            {
                Link.Left +=10;
                enemigo.Vivo = false;
                enemigo.checarEnemigo(Link, enemigo1);
                lblPuntos.Text = "Puntos: " + heroe.puntosAcu();
            }
            if (revEnemigo2Arma(Link, enemigo1, enemigo2, heroe, enemigoSeg) == true)
            {
                Link.Left -= 10;
                enemigoSeg.Vivo = false;
                enemigoSeg.checarEnemigo2(Link, enemigo2);
                lblPuntos.Text = "Puntos: " + heroe.puntosAcu();
               

            }
        }

        public static void colisionI(PictureBox Link, PictureBox enemigo1, PictureBox enemigo2, PictureBox roca1, PictureBox roca2, PictureBox roca3, PictureBox roca4, PictureBox roca5, Heroe heroe, Enemigo enemigo, Enemigo enemigoSeg, Label lblPuntos)
        {
            if (revRoca(Link, roca1, roca2, roca3, roca4, roca5) == true)
            {
                Link.Left -= 10;
            }
            if (revEnemigo(Link, enemigo1, enemigo2, heroe, enemigo, enemigoSeg) == true)
            {
                Link.Left -= 10;
            }
            if (revEnemigoArma(Link, enemigo1, enemigo2, heroe, enemigo) == true)
            {
                Link.Left -= 10;
                enemigo.Vivo = false;
                enemigo.checarEnemigo(Link, enemigo1);
                lblPuntos.Text = "Puntos: " + heroe.puntosAcu();

            }
            if (revEnemigo2Arma(Link, enemigo1, enemigo2, heroe, enemigoSeg) == true)
            {
                Link.Left -= 10;
                enemigoSeg.Vivo = false;
                enemigoSeg.checarEnemigo2(Link, enemigo2);
                lblPuntos.Text = "Puntos: " + heroe.puntosAcu();

            }
        }

        public static void colisionAbajo(PictureBox Link, PictureBox enemigo1, PictureBox enemigo2, PictureBox roca1, PictureBox roca2, PictureBox roca3, PictureBox roca4, PictureBox roca5, Heroe heroe, Enemigo enemigo, Enemigo enemigoSeg,Label lblPuntos)
        {
            if (revRoca(Link, roca1, roca2, roca3, roca4, roca5) == true)
            {
                Link.Top += 10;
            }
            if (revEnemigo(Link, enemigo1, enemigo2, heroe, enemigo, enemigoSeg) == true)
            {
                Link.Top += 10;
            }
            if (revEnemigoArma(Link, enemigo1, enemigo2, heroe, enemigo) == true)
            {
                Link.Top += 10;
                enemigo.Vivo = false;
                enemigo.checarEnemigo(Link, enemigo1);
                lblPuntos.Text = "Puntos: " + heroe.puntosAcu();

            }
            if (revEnemigo2Arma(Link, enemigo1, enemigo2, heroe, enemigoSeg) == true)
            {
                Link.Left -= 10;
                enemigoSeg.Vivo = false;
                enemigoSeg.checarEnemigo2(Link, enemigo2);
                lblPuntos.Text = "Puntos: " + heroe.puntosAcu();

            }
        }

        public static void colisionArriba(PictureBox Link, PictureBox enemigo1, PictureBox enemigo2, PictureBox roca1, PictureBox roca2, PictureBox roca3, PictureBox roca4, PictureBox roca5, Heroe heroe, Enemigo enemigo,Enemigo enemigoSeg, Label lblPuntos)
        {
            if (revRoca(Link, roca1, roca2, roca3, roca4, roca5) == true)
            {
                Link.Top -= 10;
            }
            if (revEnemigo(Link, enemigo1, enemigo2, heroe, enemigo, enemigoSeg) == true)
            {
                Link.Top -= 10;
            }
            if (revEnemigoArma(Link, enemigo1, enemigo2, heroe, enemigo) == true)
            {
                Link.Top -= 10;
                enemigo.Vivo = false;
                enemigo.checarEnemigo(Link, enemigo1);
                lblPuntos.Text = "Puntos: " + heroe.puntosAcu();

            }
            if (revEnemigo2Arma(Link, enemigo1, enemigo2, heroe, enemigoSeg) == true)
            {
                Link.Left -= 10;
                enemigoSeg.Vivo = false;
                enemigoSeg.checarEnemigo2(Link, enemigo2);
                lblPuntos.Text = "Puntos: " + heroe.puntosAcu();

            }
        }
    }
}
