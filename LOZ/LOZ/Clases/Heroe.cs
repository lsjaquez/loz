﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LOZ;
using System.Windows.Forms;

namespace LOZ.Clases
{
    class Heroe
    {
        bool arma;
        int puntos;

        public Heroe()
        {
            Arma = false;
            Puntos = 0;
    
        }

        public bool Arma { get => arma; set => arma = value; }
        public int Puntos { get => puntos; set => puntos = value; }

        public bool armarHeroe()
        {
            arma = true;
            return arma;
        }

        public int puntosAcu()
        {
            return puntos += 10;
        }

        public void Mover(object sender, KeyEventArgs e, PictureBox Link, PictureBox roca1, PictureBox roca2, PictureBox roca3, PictureBox roca4, PictureBox roca5, PictureBox enemigo1, PictureBox enemigo2, Label lblPuntos, Enemigo enemigo, Enemigo enemigoSeg, Heroe heroe, PictureBox espada, Label lblArmado, Label lblInstrucciones)
        {
            if (e.KeyCode == Keys.Left)
            {
                Link.Left -= 10;
                Reglas.colisionD(Link, enemigo1, enemigo2, roca1,roca2,roca3,roca4,roca5, heroe, enemigo,enemigoSeg, lblPuntos);

            }else if (e.KeyCode == Keys.Right)
            {
                Link.Left += 10;
                Reglas.colisionI(Link, enemigo1, enemigo2, roca1,roca2,roca3,roca4,roca5, heroe, enemigo, enemigoSeg, lblPuntos);

            }else if (e.KeyCode == Keys.Up)
            {
                Link.Top -= 10;
                Reglas.colisionAbajo(Link, enemigo1, enemigo2, roca1, roca2, roca3, roca4, roca5, heroe, enemigo, enemigoSeg,lblPuntos);

            }else if (e.KeyCode == Keys.Down)
            {
                Link.Top += 10;
                Reglas.colisionArriba(Link, enemigo1, enemigo2, roca1, roca2, roca3, roca4, roca5, heroe, enemigo, enemigoSeg, lblPuntos);

            }
            Reglas.heroePerdido(Link);
            Reglas.recogerArma(sender, e, Link, espada, heroe, lblArmado, lblInstrucciones);
        }
    }
}
