﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LOZ.Clases;

namespace LOZ
{
    public partial class mapa : Form
    {
        Heroe heroe = new Heroe();
        Enemigo enemigo = new Enemigo();
        Enemigo enemigoSeg = new Enemigo();
        Reglas reglas = new Reglas();
        public mapa()
        {
            InitializeComponent();
            lblPuntos.Text = "Puntos: " + heroe.Puntos;
            lblInstrucciones.Text = "Busca un arma";
            lblArmado.Text = "Heroe: Desarmado";
           
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            
            heroe.Mover(sender, e, Link, roca1,roca2,roca3,roca4,roca5, enemigo1, enemigo2, lblPuntos, enemigo, enemigoSeg, heroe, espada,lblArmado, lblInstrucciones);
            reglas.checarGanador(heroe, lblInstrucciones);
            
        }
    }
}
